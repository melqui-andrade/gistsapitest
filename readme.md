# Gists API Tests

This project was developed in Java using Maven and IntelliJ IDE. So to run the project you need install all requirements for those tools first.

Here we have six test case:
- Create a gists;
- List gists;
- Get a existent gists;
- Get a non-existent gists;
- Update a gists;
- Delete a gists.

For each test I validate the status code, and its response schema. All the schemas are in "resource" file.
To you be able to create a gist, you need insert your token with the permission to create them in the "RequestSpecification" method. My token will expire soon, so maybe you need generate your token.

In the update test I check if the updated information is presented in the response.

The test show a bug in the Get a non-existent gists. The response have a empty body but the status code was 200. In the documentation they said the correct one is 404.
