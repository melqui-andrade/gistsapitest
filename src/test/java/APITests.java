import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static org.hamcrest.Matchers.is;
import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static io.restassured.RestAssured.given;

public class APITests{

    private static String created_gists_id = "";

    @Before
    public void setup(){
        RestAssured.baseURI = "https://api.github.com/";
        RestAssured.useRelaxedHTTPSValidation();
    }

    @Test
    public void validate_create_gist(){
        JSONObject requestBody = new JSONObject();
        requestBody.put("description", "Test description");
        requestBody.put("public", false);

        JSONObject filesObj = new JSONObject();
        JSONObject readmeObj = new JSONObject();
        readmeObj.put("content", "Some description in readme file");
        filesObj.put("README.md", readmeObj);
        requestBody.put("files", filesObj);

        File createGistSchema = new File("src/test/resource/post_gists_schema.json");

        this.created_gists_id = authenticatedResquest()
                .when()
                .body(requestBody.toJSONString())
                .post("gists")
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .and()
                .assertThat()
                .body(JsonSchemaValidator.matchesJsonSchema(createGistSchema))
                .extract().body().path("id");
        System.out.println(this.created_gists_id);

    }

    @Test
    public void validate_get_list_gist(){

        File getGistSchema = new File("src/test/resource/get_list_gists_schema.json");

        Response response = authenticatedResquest()
                .param("since", "2023-03-16T00:00:00Z")
                .param("per_page", 3)
                .when()
                .get("gists")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .assertThat()
                .body(JsonSchemaValidator.matchesJsonSchema(getGistSchema))
                .extract().response();
        System.out.println(response.asPrettyString());

    }

    @Test
    public void validate_get_non_existent_gist(){


        authenticatedResquest()
                .param("aaaaaa")
                .when()
                .get("gists")
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND);

    }

    @Test
    public void validate_get_gists(){
        if(!created_gists_id.isEmpty()){
            File getGistSchema = new File("src/test/resource/get_a_gists_schema.json");
            authenticatedResquest()
                    .param(created_gists_id)
                    .when()
                    .get("gists/")
                    .then()
                    .statusCode(HttpStatus.SC_OK)
                    .and()
                    .assertThat()
                    .body(JsonSchemaValidator.matchesJsonSchema(getGistSchema));
        }
    }

    @Test
    public void validate_update_gists(){
        if(!created_gists_id.isEmpty()){
            JSONObject requestBody = new JSONObject();
            requestBody.put("description", "Test description updated!");
            requestBody.put("public", false);

            JSONObject filesObj = new JSONObject();
            JSONObject readmeObj = new JSONObject();
            readmeObj.put("content", "Some description in readme file");
            filesObj.put("README.md", readmeObj);
            requestBody.put("files", filesObj);

            File getGistSchema = new File("src/test/resource/update_gists_schema.json");
            authenticatedResquest()
                    .param(created_gists_id)
                    .when()
                    .body(requestBody.toJSONString())
                    .patch("gists/")
                    .then()
                    .statusCode(HttpStatus.SC_OK)
                    .and()
                    .assertThat()
                    .body(JsonSchemaValidator.matchesJsonSchema(getGistSchema))
                    .and()
                    .body("description", is(requestBody.get("description")));
        }
    }

    @Test
    public void validate_delete_gists(){
        System.out.println(created_gists_id);
        if(!created_gists_id.isEmpty()){
            authenticatedResquest()
                    .param(created_gists_id)
                    .when()
                    .delete("gists/")
                    .then()
                    .statusCode(HttpStatus.SC_NO_CONTENT);

        }
        else {
            Assert.fail("Gists ID null");
        }
    }



    private RequestSpecification authenticatedResquest(){
        return given()
                .contentType("application/json")
                .auth()
                .oauth2("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")
                .log().all();
    }


}
